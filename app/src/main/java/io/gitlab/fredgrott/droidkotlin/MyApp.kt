/* Copyright 2019 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package io.gitlab.fredgrott.droidkotlin

import android.app.Application
import android.content.Context
import android.util.Log
import android.util.Log.INFO
import androidx.multidex.MultiDex
import com.github.ajalt.timberkt.Timber
import timber.log.Timber.DebugTree

class MyApp : Application() {

    val Context.myApp: MyApp
        get() = applicationContext as MyApp

    override fun onCreate() {
        super.onCreate()
        // see https://fabric.io/kits/android/crashlytics/install
        // Fabric.with(this, new CrashAnalytics())
        // }

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }
    override fun attachBaseContext(base: Context) {

        super.attachBaseContext(base )
        MultiDex.install(this)
    }
    private class CrashReportingTree : timber.log.Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        }

        fun isLoggable(priority: Int, tag: String?): Boolean {
            return priority >= INFO
        }

        protected fun log(priority: Int, tag: String, t: Throwable?, message: String) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }

            FakeCrashLibrary.log(priority, tag, message)

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t)
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t)
                }
            }
        }
    }
}