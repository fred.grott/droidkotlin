# DroidKotlin

![devops](images/ci-cd-devops.png)

![droidgradle](images/androidgradle.png)![androidkotlin](images/androidkotlin.png)![androidstudio](images/androidstudio.png)

My build and gitlab ci setup for fast prototyping android library and android app solutions and one half of the totla developer approach to full bug free totally reposnive and testable android apps.

# What Is Devops?

I am making expert developer choices of what app architecture and programming methods to use coupled to 
pair with the right operations infrastructure to reduce development time producing milesotnes and features 
and tracking down hard to fix bugs such as app freezes and state bugs.

The benefit to your startup is a more dependable time line of agile developing an android application and less time 
tracking down and fixing bugs while the end android device users gets a rock-solid stable cool looking-UI-UX android application 
that they will crow to their friends, hey you got to get this app.

# Overall Developer Philosophy

My approach is somewhat different in that I reexamine choices and decisions as 
far as tradeoffs etc as I follow the concept of approximate truth rather than aboslute 
truth about the World and the Universe.

As far as debugging I am my biggest skeptic wwhich helps me find bugs and their 
solutions faster than the arrogant devs you may be used to.

# Functional Programmping vs OOP

I am moving to a functioal programming oriented programming approach. Its not 
purely functional yet as the Kotlin I write has to interop with the Android 
java libraries but its as close as I can get.

That specifically means I am moving away from OOP and SOLID and towards FOP and 
Monads(math terms its Monoids) better lambdas than java8 has and other 
fucitonal goodies.

The CleanArchitecture setup I am abusing and using is 
DomainState-View-ViewState along with Domain, IO. Context, FreeMonads, and
UI and of course ChildAppController as the class that extends he app class.

# End-Of-2019 Migration

I fully expect with the upcoming sites switching to tsl1.2 atr end of 2019 including 
browsers to causse Google to deepsix android devices that support api21 through api25 by 
cutting them off form PlayStore App updates. At that time I expect to migrate this setup 
to api26 and up and enable the errorprone plguins for better compiler checking.

## Developer Choices

### Event Programming, Event Streaming(RXjava) and State App Arch Redux

I have chosen to blaze a trail using the new event programming implementaiton of 
Reactive combined with Redux as it covers the full range of responsiveness ie no app
freezes along with getting rid of most of the state bugs that are always the hardest to fix and 
take the longest in dev time to fix.

To ensure that my views have no dependencies I couple my redux app architecture approach with 
layering component layers according to cleanarch guidelines and use dagger for 
dependency injection as than due to the vview having no dependencies I reach the 
Niravana of having a fully testable android app.


## Operations Choices

### Instrumented Testing

Emulators of android has moved to non arm images which means that any ci-shared 
runner will not have access to kvm required to enable hardware acceleration required for the 
emulator. At the same time android device cloud testing labs do not allow batching tests among 
a group of emulators.

So I do the contrainian choice of using Jke Wharton's Spoon tooling to batch test among 
many physical or virtual devices in parallel to save testing time.

### BDD AND Regression Testing

BDD and Regression testing is done via my ci-server confgi script settings as ti can be executed without an android 
device or emulator and its somewhat desired to have those tests run every time a commit is made,


# First Steps

If you are re-using this for your own stuff, after you setup the files and 
customize them execute  for each project module

```
gradlew projectmodule:lintDebug
```

```
gradlew projectmodule:detektbaseline
```

Than pretty much you are all set to start coding.

# Dev Details 



This is more for those devs who want to re-use this with their own
projects, Arcane Java, Kotlin, and Android tech terms ahead.

## Monoids oh  my

Yes, I use monoids. Its a way to structure functional programming. Useful in 
writing tests and especially makes it easier to write reactive code aand redux code.

Library details ere here

https://github.com/kittinunf/Result

Background on Monoids is here

https://fsharpforfunandprofit.com/posts/monoids-without-tears/

## Gitignore for Android Studio

Details of why its set that way is here

https://proandroiddev.com/deep-dive-into-idea-folder-in-android-studio-53f867cf7b70




## Build Times Godot

Details here

https://github.com/hannesstruss/godot

after a few builds run 

```
gradlew projectmodule:generateGodotReport
```



## Versioning and GitSha

I use a combination of Jake Wharton's lazy android versioning along with the 
grgit gradle plugin to get the git sha details on MS Windows as JGit which grgit plugin 
uses is cross platform so it always works no matter what platform I am on.

The semantic versioning guidelines that I follow are here

https://semver.org/

## Library Publishing

Its handled via the dcents plugin and Jitpack and directions aare here 

https://jitpack.io/docs/ANDROID/

## DebugBottle

i use debug bottle details how to inject the debug drawer into debug builds is here

https://github.com/kiruto/debug-bottle



## Gradle Versions File

I am using the new versions.gradle set up format Google seems to 
love as it makes it easier to update deps via the project 
structure panel in Android Studio.

But, be careful you still have to watch to make sure it updates variables 
instead of attempting to update the dependency full entry.

## API Key Security

By default I only run production builds in non cloud envs with 
a secured androoid apk key.

To secure other api keys one uses the BuildConfigField res setup 
in combination with the gradle build properties plugin setup here

https://github.com/novoda/gradle-build-properties-plugin

Keep in mind that some api keys cannot be obfuscated that way such as 
the Google map key and thus you have to use the BuildConfigField res 
arrangement alone to obfuscate the mapp key from those who 
reverse engineer apk files.

## Code QA Warning Suppressions

### SpotBugs

Using Java's SuppressWarnings and Kotlin's Suppress annoations as the fb jsr305 requires 
java8 and so does the spotbugs annotation dependency

### Detekt

Use Java's SuppressWarnings or Kotlin's Suppress annotations.

## Code Coverage

Jacoco is setup to do code coverage on both TDD and BDD testing with 
on TDD testing Spoon coverage results are used to generate the coeverage report 
if Spoon is used.

## Mocking

Mockito's inline-Mockmaker is too slow for tdd instrumented tesing so I use the 
jetbrains all-open plugin and some annotation magic to make sure its only 
open for testing.

Uses the OpenForTesting annotation on production code

```
@OpenForTesting
```

see

https://proandroiddev.com/mocking-androidtest-in-kotlin-51f0a603d500


### Mockito-Kotlin

I use a mockito-kotlin plugin to get kotlin syntax use wiki is here

https://github.com/nhaarman/mockito-kotlin/wiki


## Test Assertions

I use the Kotlin tTst library for test assertions, details are here

https://kotlinlang.org/api/latest/kotlin.test/kotlin.test/index.html

## TDD Test DSL Usage

How to use AndroidKtx test dsl is at

https://github.com/codecentric/androidtestktx

## BDD SetUp

Spek BDD framework is being used with their backported junit4 support.

For Spek tests generally follow this but ignore setting up interfaces and 
test doubles as with reactive and redux its not needed

https://medium.com/@jozemberi/developing-with-kotlin-and-testing-with-spek-d69a94857d


## Logging

### DebugLogging

I use a temp hugo contrib lib and its the same Hugo annotation of

```
@DebugLog
```

https://github.com/JakeWharton/hugo

### Logging with Timber via Timberkt

Same setup as timber, sample debug logging tree code in the app 
project module. just follow the code sample

https://github.com/ajalt/timberkt/blob/master/sample/src/main/kotlin/com/github/ajalt/timberkt/sample/MainActivity.kt


## App Logging

App logging can be implemented via the RxRedux middleware api.


## Multidex

Multidex is enabled by default, so need to change anything as using reactive we will 
be at 65k methods anyway.

## GitLab Runner Cpnfig Setup

Docs are here 

https://docs.gitlab.com/runner/


# Credits


Fred Grott



# About

You can find out more about me at:

https://gitlab.com/fred.grott/myportfolio

# License

Copyright 2019 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


# Further Reaading

## Android DevOps

https://blog.undabot.com/the-art-of-android-devops-fa29396bc9ee

https://hackernoon.com/role-of-devops-in-mobile-app-development-mobile-devops-25e684fcd52e

http://jeremie-martinez.com/2016/01/14/devops-on-android/

